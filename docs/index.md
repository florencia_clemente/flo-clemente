## Hola...

Este es el portafolio del posgrado en Fabricación Digital.

Por ahora esta página de bienvenida parece medio pobre, es verdad, pero irá tomando color a medida que pasen las semanas y vaya llegando la inspiración y el contenido.

Va quedando algo de info en las secciones que siguen:

- [About](https://florencia_clemente.gitlab.io/flo-clemente/about/about/): un poco sobre mí y el por qué de este curso
- [Módulos](https://florencia_clemente.gitlab.io/flo-clemente/modulos/MP01/): donde se irán documentando los avances
- [Proyecto](https://florencia_clemente.gitlab.io/flo-clemente/proyecto/desarrollo/): acá aún no hay nada... ya vendrán cosas interesantes


> Próximo desafío:
>
> Estética general, colores y tipos de letra.


Saludos!

Flo
