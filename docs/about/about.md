# Sobre mi

[comment]: <> (![](../images/perezoso.jpg))

Hola, soy Florencia, y así es como me presento en LinkedIn.

>Docente adjunto de alta dedicación en la Universidad Católica del Uruguay desde hace 10 años, especializada en emprendedurismo e innovación. Es ingeniera en Telecomunicaciones con una Maestría en Comunicaciones Inalámbricas de la Universidad de Santa Clara, y un MBA de la UCU.
>
>Se desempeña como docente en el Centro Íthaka de Emprendimiento e Innovación, acompañando emprendedores en el desarrollo de sus proyectos. Es responsable de cursos de innovación y emprendimientos en español y en inglés, con foco en la creación y validación de modelos de negocio innovadores para la Facultad de Ciencias Empresariales, así como también de materias en la Facultad de Ingenieria y Tecnologías donde es tutora de trabajos finales de grado.

Además de las formalidades anteriores, soy mamá de Nico, con quien descubrimos (entre muchas cosas) una necesidad de todos los padres y madres, que se transformó en una idea de negocio y de allí un emprendimiento. Así que nos convertimos en la familia emprendedora detrás de Toys4Us, una propuesta de triple impacto cuyo objetivo es democratizar el acceso a materiales didácticos de los métodos Montessori, Pikler y Waldorf.

Los invito a conocer la propuesta en la [WEB](https://www.toys4us.com.uy/), o donde se da toda la acción que es en el [INSTAGRAM](https://www.instagram.com/toys4us.uy/).

### ¿Fabricación digital?

Soy adicta al estudio, el conocimiento y los nuevos desafíos. Así pasamos de alquilar juguetes, a fabricarlos, y en si todo sale bien en un futuro cercano a crear productos innovadores y desafiantes.

De ahí la necesidad de contar con herramientas para que los nuevos Juguetones queden divinos, funcionales y re divertidos.
